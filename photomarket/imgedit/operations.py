from multiprocessing import Process, Queue, cpu_count

from enum import Enum

import numpy as np


class DIRECTION(Enum):
    LEFT = 0
    RIGHT = 1
    HORIZONTAL = 2
    VERTICAL = 3


def rotate(image: np.ndarray, direction=DIRECTION.RIGHT) -> np.ndarray:
    """
    Rotate image in direction specified by parameter
    :param image: original image, can be modified!
    :param direction: allowed values: DIRECTION.RIGHT, DIRECTION.LEFT
    """
    k = 3 if direction == DIRECTION.RIGHT else 1
    return np.rot90(image, k)


def negative(image: np.ndarray) -> np.ndarray:
    """
    Invert colors
    :param image: original image, can be modified!
    """
    # Leaving [:,:,4] alone in case of transparent image
    image[:, :, :3] = (255 - np.uint8(image[:, :, :3]))
    return image


def grey(image: np.ndarray) -> np.ndarray:
    """
    Translate image to greyscale
    :param image: original image, can be modified!
    """
    image_gray = np.sum([0.299, 0.587, 0.114] * image[:, :, :3], axis=2)
    image[:, :, :3] = image_gray[:, :, np.newaxis]
    return image


def adjust_brightness(image: np.ndarray, multiply: float = 2) -> np.ndarray:
    """
    Multiply brightness by given constant
    :param image: original image, can be modified!
    :param multiply: float constant
    """
    res = np.clip(np.int16(image[:, :, :3]) * multiply, 0, 255)
    image[:, :, :3] = res
    return image


def mirror(image: np.ndarray,
           direction: DIRECTION = DIRECTION.HORIZONTAL) -> np.ndarray:
    """
    Flip image along horizontal or vertical axis
    :param image: original image, can be modified!
    :param direction: allowed values: DIRECTION.HORIZONTAL, DIRECTION.VERTICAL
    """
    if direction == DIRECTION.HORIZONTAL:
        return rotate(np.transpose(image, axes=(1, 0, 2)))
    else:
        return np.transpose(rotate(image), axes=(1, 0, 2))


class FilterJob:
    """
    Data class for reconstructing final image after parallel processing
    """
    def __init__(self, image, t_r_orig, b_r_orig, c_top, c_bottom):
        self.image_data = image
        self.top_orig = t_r_orig
        self.bottom_orig = b_r_orig
        self.res_cut_top = c_top
        self.res_cut_bottom = c_bottom


def _apply_filter(image: np.ndarray, kernel: np.ndarray, multiply: float):
    """
    Parallel processing of kernel convolution
    Filter doesn't get applied to edges of the image, for correct result, you should
    crop the image by the kernel radius from all sides
    :param image: original image, can be modified!
    :param kernel: filtering kernel
    :param multiply: filtering multiplication constant
    :return: image with applied filter
    """
    n_processes = cpu_count() * 2
    k_radius = kernel.shape[0] // 2
    part_size = np.math.ceil(image.shape[0] / n_processes)
    queue = Queue()
    processes = []
    for i in range(n_processes):
        top_row_orig = part_size * i
        top_row = top_row_orig - k_radius
        bottom_row_orig = part_size * (i + 1)
        bottom_row = bottom_row_orig + k_radius
        res_cut_bottom = res_cut_top = k_radius

        if i == 0:
            top_row = 0
            top_row_orig = 0
            res_cut_top = 0
        elif i == n_processes - 1:
            bottom_row = image.shape[0]
            bottom_row_orig = image.shape[0]
            res_cut_bottom = 0

        job = FilterJob(image[top_row:bottom_row], top_row_orig, bottom_row_orig, res_cut_top, res_cut_bottom)
        processes.append(Process(target=_apply_filter_thr, args=(job, kernel, multiply, queue)))

    for process in processes:
        process.start()

    for _ in processes:
        r: FilterJob = queue.get()
        image[r.top_orig:r.bottom_orig] = r.image_data[r.res_cut_top:r.image_data.shape[0] - r.res_cut_bottom]

    for process in processes:
        process.join()

    return image


def _apply_filter_thr(job: FilterJob, kernel: np.ndarray, multiply: float, queue: Queue):
    """
    :param job: assignment containing to process
    :param kernel: filter kernel matrix
    :param multiply: float constant
    :param queue: shared queue for multiple processes for submitting result
    :return:
    """
    image = job.image_data
    tmp_res = image.copy()
    h, w, _ = image.shape
    k_radius = kernel.shape[0] // 2
    for i in range(h):
        for j in range(w):
            if i < k_radius or i > h - (k_radius + 1) or j < k_radius or j > w - (k_radius + 1):
                continue
            area = image[i - k_radius:i + k_radius + 1, j - k_radius:j + k_radius + 1, :3]
            tmp_res[i, j, 0] = np.clip(np.sum(area[:, :, 0] * kernel * multiply), 0, 255)
            tmp_res[i, j, 1] = np.clip(np.sum(area[:, :, 1] * kernel * multiply), 0, 255)
            tmp_res[i, j, 2] = np.clip(np.sum(area[:, :, 2] * kernel * multiply), 0, 255)
    job.image_data = tmp_res
    queue.put(job)


def sharpen(image: np.ndarray) -> np.ndarray:
    """
    Apply sharpening filter to image
    :param image: original image, can be modified!
    """
    return _apply_filter(image,
                         np.array([[0, -1, 0],
                                   [-1, 5, -1],
                                   [0, -1, 0]]),
                         1)


def blur(image: np.ndarray) -> np.ndarray:
    """
    Apply blurring filter to image
    :param image: original image, can be modified!
    """
    return _apply_filter(image,
                         np.array([[1, 1, 1],
                                   [1, 1, 1],
                                   [1, 1, 1]]),
                         1 / 9)


def edges(image: np.ndarray) -> np.ndarray:
    """
    Apply edge detection filter to image
    :param image: original image, can be modified!
    """
    return _apply_filter(image,
                         np.array([[0, 0, -1, 0, 0],
                                   [0, -1, -2, -1, 0],
                                   [-1, -2, 16, -2, -1],
                                   [0, -1, -2, -1, 0],
                                   [0, 0, -1, 0, 0]]),
                         1)
