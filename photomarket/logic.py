from imgedit import operations
from imgedit.operations import DIRECTION
from view import View
import numpy as np
from PIL import Image
from tkinter import filedialog


def update(fn):
    """
    Decorator that updates view with a new image
    """

    def wrapper(self, *args, **kwargs):
        res = fn(self, *args, **kwargs)
        self.view.update_image(self.img)
        return res

    return wrapper


class Application:

    def __init__(self):
        self.view = View()
        self.img = np.asarray(Image.open('examples/nova-budova.jpg'), dtype=np.float)
        self.view.update_image(self.img)
        # self.view.register_event_handler('load', '')
        self.view.set_handler('file:open', self.handle_file_open)
        self.view.set_handler('file:save', self.handle_file_save)
        self.view.set_handler('filter:sharpen', self.handle_edit_sharpen)
        self.view.set_handler('filter:blur', self.handle_edit_blur)
        self.view.set_handler('filter:edges', self.handle_edit_edges)
        self.view.set_handler('edit:increase brightness', self.handle_edit_inc_brightness)
        self.view.set_handler('edit:decrease brightness', self.handle_edit_dec_brightness)
        self.view.set_handler('edit:negative', self.handle_edit_negative)
        self.view.set_handler('edit:greyscale', self.handle_edit_grey)
        self.view.set_handler('transform:rotate left', lambda: self.handle_transform_rotate(DIRECTION.LEFT))
        self.view.set_handler('transform:rotate right', lambda: self.handle_transform_rotate(DIRECTION.RIGHT))
        self.view.set_handler('transform:mirror horizontal', lambda: self.handle_mirror(DIRECTION.HORIZONTAL))
        self.view.set_handler('transform:mirror vertical', lambda: self.handle_mirror(DIRECTION.VERTICAL))
        self.view.mainloop()

    @update
    def handle_file_open(self):
        """
        Displays file open dialog and loads selected file
        """
        res = filedialog.askopenfile(filetypes=[('JPEG', '*.jpg'), ('PNG', '*.png'), ('BMP', '*.bmp')])
        if not res:
            return
        image = np.asarray(Image.open(res.name), dtype=np.float)
        self.img = image

    def handle_file_save(self):
        """
        Displays file save dialog and saves file
        """
        res = filedialog.asksaveasfile(filetypes=[('JPEG', '*.jpg'), ('PNG', '*.png'), ('BMP', '*.bmp')])
        if not res:
            return
        img = Image.fromarray(np.uint8(self.img))
        img.save(res.name)

    @update
    def handle_edit_sharpen(self):
        self.img = operations.sharpen(self.img)

    @update
    def handle_edit_blur(self):
        self.img = operations.blur(self.img)

    @update
    def handle_edit_edges(self):
        self.img = operations.edges(self.img)

    @update
    def handle_edit_inc_brightness(self):
        self.img = operations.adjust_brightness(self.img, 1.5)

    @update
    def handle_edit_dec_brightness(self):
        self.img = operations.adjust_brightness(self.img, .5)

    @update
    def handle_edit_negative(self):
        self.img = operations.negative(self.img)

    @update
    def handle_edit_grey(self):
        self.img = operations.grey(self.img)

    @update
    def handle_transform_rotate(self, direction):
        self.img = operations.rotate(self.img, direction)

    @update
    def handle_mirror(self, direction):
        self.img = operations.mirror(self.img, direction)
