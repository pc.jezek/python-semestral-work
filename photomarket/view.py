from tkinter import Tk, Menu
import tkinter as tk
import numpy as np
from PIL import Image, ImageTk


class View:
    def create_menu(self):
        """
        Create menu items from menu_items dictionary
        :return: created menu
        """
        menu = Menu(self.window)
        for top_menu, sub_menus in self.menu_items.items():
            menu_template = Menu(menu, tearoff=False)
            for sub_menu in sub_menus:
                menu_template.add_command(label=sub_menu,
                                          command=lambda t=top_menu, s=sub_menu: self.handle_command(t, s))
            menu.add_cascade(label=top_menu, menu=menu_template)
        return menu

    def __init__(self):
        self.menu_items = {'File': ['Open', 'Save'],
                           'Edit': ['Negative', 'Increase Brightness', 'Decrease Brightness', 'Greyscale'],
                           'Filter': ['Sharpen', 'Blur', 'Edges'],
                           'Transform': ['Rotate Left', 'Rotate Right', 'Mirror Horizontal', 'Mirror Vertical']}
        self.handlers = {f'{k.lower()}:{item.lower()}': lambda: None
                         for k, v in self.menu_items.items() for item in v}
        # Handlers are registered under <string, function> dictionary with keys 'category:command'
        self.window = Tk()
        self.window.title('PhotoMarket')
        self.width = self.height = 600
        self.window.geometry(f'{self.width}x{self.height}')
        self.original_image = None

        self.label = tk.Label(self.window)
        # Resize image with window
        self.label.bind('<Configure>', lambda e: self._resize(e.width, e.height))
        self.label.pack(side="bottom", fill="both", expand="yes")

        self.window.configure(menu=self.create_menu())

    def mainloop(self):
        self.window.mainloop()

    def update_image(self, image: np.ndarray):
        """
        Display image and resize to window size (keep proportions)
        :param image: to display
        """
        pil_img = Image.fromarray(np.uint8(image))
        self.original_image = pil_img.copy()

        tk_img = ImageTk.PhotoImage(pil_img)
        self.label.configure(image=tk_img)
        self.label.image = tk_img
        self._resize(self.width, self.height)

    def handle_command(self, top_menu, sub_menu):
        """
        Handles menu click, runs registered handler
        :param top_menu: first part of handler key
        :param sub_menu: second part of handler key
        """
        self.window.configure(cursor="watch")
        self.window.update()
        self.handlers[f'{top_menu.lower()}:{sub_menu.lower()}']()
        self.window.configure(cursor="")

    def _resize(self, w_width, w_height):
        """
        Resize image to width and height given by parameters
        Keep proportions (leaving white strips at the sides
        if aspect ratio is different than the window's)
        """
        self.width = w_width
        self.height = w_height
        width = w_width
        width_ratio = width / self.original_image.width
        height = int(self.original_image.height * width_ratio)

        if height >= w_height:
            height_ratio = w_height / height
            height = w_height
            width = int(width * height_ratio)

        resized = self.original_image.resize((width, height), Image.ANTIALIAS)
        tk_img = ImageTk.PhotoImage(resized)
        self.label.configure(image=tk_img)
        self.label.image = tk_img

    def set_handler(self, keyword: str, handler: ()):
        """
        Register handler for menu item
        :param keyword: menu item keyword
        :param handler function to execute on click
        """
        self.handlers[keyword] = handler
