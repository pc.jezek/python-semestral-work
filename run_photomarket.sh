#!/bin/bash

function print_requirements () {
    echo 'Please install following packages using "pip install" or "conda install":'
    echo "numpy"
    echo "pillow"
    echo "tkinter"
}

python -c '
try:
    import numpy
    import PIL
    import tkinter
    import enum
except ModuleNotFoundError as e:
    print("Error:", e.name, "not found.")
    exit(1)' && python photomarket || print_requirements

